package br.com.capco.cadastroAgencia.controller;

import br.com.capco.cadastroAgencia.controller.dto.AgenciaDto;
import br.com.capco.cadastroAgencia.modelo.Agencia;
import br.com.capco.cadastroAgencia.repository.AgenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/agencias")
public class CadastroAgenciaController {

    @Autowired
    private AgenciaRepository repository;

//   LISTA TODAS AGÊNCIAS CADASTRADAS
    @GetMapping
    public List<AgenciaDto> listarComOrdemCrescenteDeNumeroAgencia(){
        List<Agencia> all = repository.findAll(Sort.by(Sort.Direction.ASC, "numeroAgencia"));
        List<AgenciaDto> allDto = new ArrayList<>();
        all.forEach(a -> allDto.add(new AgenciaDto(a)));

        return allDto;
    }

    // RECUPERA PELO NUMERO AGENCIA
    @GetMapping("/{numeroAgencia}")
    public ResponseEntity recuperarPorNumeroAgencia(@PathVariable("numeroAgencia") Integer numeroAgencia){
        Optional<Agencia> agenciaDB = repository.findOneByNumeroAgencia(numeroAgencia);
        if(agenciaDB.isPresent()) {
            return ResponseEntity.ok(new AgenciaDto(agenciaDB.get()));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

//    CADASTRAR UMA NOVA AGENCIA
    @PostMapping
    public ResponseEntity novo(@Valid @RequestBody AgenciaDto agencia) {

        Optional<Agencia> agenciaDB = repository.findOneByNumeroAgencia(agencia.getNumeroAgencia());
        if(agenciaDB.isPresent()) {
            return ResponseEntity.badRequest().body("Numero de agencia ja cadastrado.");
        } else {
            Agencia saved = repository.save(agencia.toAgencia());
            return ResponseEntity.ok(new AgenciaDto(saved));
        }
    }

    //    ATUALIZAR UMA AGENCIA
    @PostMapping("/{numeroAgencia}")
    public ResponseEntity atualizar(@PathVariable("numeroAgencia") Integer numeroAgencia, @Valid @RequestBody AgenciaDto dadosAtualizados) {

        Optional<Agencia> agenciaDB = repository.findOneByNumeroAgencia(numeroAgencia);
        if(agenciaDB.isPresent()) {
            Agencia atualizado = agenciaDB.get();
            if(dadosAtualizados.getNumeroAgencia() != null && !dadosAtualizados.getNumeroAgencia().equals(numeroAgencia)) {
                Optional<Agencia> agenciaComNumeroIgual = repository.findOneByNumeroAgencia(dadosAtualizados.getNumeroAgencia());
                if(agenciaComNumeroIgual.isPresent()) {
                    return ResponseEntity.badRequest().body("Numero de agencia ja cadastrado.");
                } else {
                    atualizado.setNumeroAgencia(dadosAtualizados.getNumeroAgencia());
                }
            }
            atualizado.setTelefone(dadosAtualizados.getTelefone());
            atualizado.setEndereco(dadosAtualizados.getEndereco().toEndereco());

            Agencia saved = repository.save(atualizado);
            return ResponseEntity.ok(new AgenciaDto(saved));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

//    DELETA UMA AGENCIA
    @DeleteMapping("/{numeroAgencia}")
    public ResponseEntity deleteByNumeroAgencia(@PathVariable ("numeroAgencia") Integer numeroAgencia){

        Optional<Agencia> agenciaDB = repository.findOneByNumeroAgencia(numeroAgencia);

        if(agenciaDB.isPresent()) {
            repository.delete(agenciaDB.get());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
