package br.com.capco.cadastroAgencia.controller.dto;

import br.com.capco.cadastroAgencia.modelo.Endereco;

import java.io.Serializable;

public class EnderecoDto implements Serializable {

    private String rua;
    private int numeroEndereco;
    private String bairro;
    private String cidade;
    private String estado;

    public EnderecoDto() {}

    public EnderecoDto(Endereco endereco) {
        this.rua = endereco.getRua();
        this.numeroEndereco = endereco.getNumeroEndereco();
        this.bairro = endereco.getBairro();
        this.cidade = endereco.getCidade();
        this.estado = endereco.getEstado();
    }

    public Endereco toEndereco() {
        return new Endereco(this.rua, this.numeroEndereco, this.bairro, this.cidade, this.estado);
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public int getNumeroEndereco() {
        return numeroEndereco;
    }

    public void setNumeroEndereco(int numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
