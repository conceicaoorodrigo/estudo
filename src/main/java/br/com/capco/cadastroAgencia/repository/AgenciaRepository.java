package br.com.capco.cadastroAgencia.repository;

import br.com.capco.cadastroAgencia.modelo.Agencia;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AgenciaRepository extends JpaRepository<Agencia, Integer> {

    Optional<Agencia> findOneByNumeroAgencia(Integer numeroAgencia);

}
